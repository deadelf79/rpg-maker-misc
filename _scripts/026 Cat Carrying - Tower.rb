# Это не "абстрактный", а вполне конкретный класс стопки, отображающейся
# на голове персонажа, раскачивающейся при его движениях и так далее.

class CarryStack_Tower < CarryStack_Ordered
  
  DEFAULT_COMPRESS=1
  DEFAULT_SLOT_HEIGHT=0.5
  DEFAULT_SKEW_RATE_X=0.3
  DEFAULT_SKEW_RATE_Y=0.1
  DEFAULT_SKEW_MAX_X=0.2
  DEFAULT_SKEW_MAX_Y=0.1
  DEFAULT_PIVOT_HEIGHT=0.7
  DEFAULT_STAB_PER_UPDATE_X=DEFAULT_SKEW_RATE_X/2
  DEFAULT_STAB_PER_UPDATE_Y=DEFAULT_SKEW_RATE_Y/2
  DEFAULT_OPPOSITE_DIR_RATE_MULT=5
  
  attr_accessor :pivot_height
  attr_accessor :skew_rate_x
  attr_accessor :skew_rate_y
  attr_accessor :skew_x_max
  attr_accessor :skew_y_max
  attr_accessor :stabilize_per_update_x
  attr_accessor :stabilize_per_update_y
  attr_accessor :opposite_dir_rate_mult
  attr_accessor :compress
  attr_reader   :skew_x
  attr_reader   :skew_y
  attr_reader   :carrier
  
  def initialize(carrier, max_stack=DEFAULT_MAX_STACK)
    @carrier=carrier
    @pivot_height=DEFAULT_PIVOT_HEIGHT
    @skew_x=0
    @skew_y=0
    @skew_rate_x=DEFAULT_SKEW_RATE_X
    @skew_rate_y=DEFAULT_SKEW_RATE_Y
    @skew_x_max=DEFAULT_SKEW_MAX_X
    @skew_y_max=DEFAULT_SKEW_MAX_Y
    @stabilize_per_update_x=DEFAULT_STAB_PER_UPDATE_X
    @stabilize_per_update_y=DEFAULT_STAB_PER_UPDATE_Y
    @opposite_dir_rate_mult=DEFAULT_OPPOSITE_DIR_RATE_MULT
    @compress=DEFAULT_COMPRESS
    sync_to_carrier
    super(max_stack)
  end
  
  def sync_to_carrier
    @pivot_x=@carrier.real_x
    @pivot_y=@carrier.real_y
  end
  
  def added_to_stack(thing, slot)
    thing.self_switch_on(:carried)
    adjust_carried(thing)
    thing.carried_in=self
    
    thing.charpath=CharPathManager.new(thing)
    thing.charpath.timing=Timing.new(15)
    thing.charpath.tracer=Trace_Arc.new( \
     start_point: Point_Fixed.from_char(thing), \
     end_point:   Point_Character.new(@carrier, 0, -height_by_slot(slot)), \
     height: 1 \
     )
     thing.charpath.start
    
    SceneManager.scene.spriteset.sort_character_sprites! \
     { |a, b| a.character.carried_depth <=> b.character.carried_depth }
    update_slot(slot)
  end
  
  def adjust_carried(thing)
    slot=find_slot_of(thing)
    thing.to_top(slot)
  end
  
  def expelled_from_stack(thing, former_slot)
    # WIP
    thing.carried_in=nil
    thing.self_switch_off(:carried)
  end
  
  def height_of_slot(slot)
    return 0 unless @stack[slot]
    return @stack[slot].get_param_length('height', DEFAULT_SLOT_HEIGHT)
  end
  
  def height_by_slot(slot)
    return @pivot_height if slot==0
    return height_by_slot(slot-1)+height_of_slot(slot-1)
  end
  
  def update    
    return if empty?
    if moving?
      update_moving
      sync_to_carrier
    else
      update_standing
    end
    update_slots
  end
  
  def moving?
    @pivot_x!=@carrier.real_x or @pivot_y!=@carrier.real_y
  end
  
  def skewed?
    @skew_x!=0 or @skew_y!=0
  end
  
  def max_skewed?
    @skew_x.abs>=@skew_x_max and @skew_y.abs>=@skew_y_max
  end
  
  def update_moving
    update_moving_skew
  end
  
  def update_moving_skew
    @skew_x+= \
     @skew_rate_x*(@pivot_x-@carrier.real_x) \
     * ((@skew_x.sign!=(@pivot_x-@carrier.real_x).sign) ? @opposite_dir_rate_mult : 1)
    @skew_x=@skew_x_max*@skew_x.sign if @skew_x.abs>@skew_x_max
    @skew_y+= \
     @skew_rate_y*(@pivot_y-@carrier.real_y) \
     *((@skew_y.sign!=(@pivot_y-@carrier.real_y).sign) ? @opposite_dir_rate_mult : 1)
    @skew_y=@skew_y_max*@skew_y.sign if @skew_y.abs>@skew_y_max
  end
  
  def update_standing
    update_standing_skew
  end
  
  def update_standing_skew
    if skewed? then
      if @skew_x>0 then
        @skew_x=[@skew_x-@stabilize_per_update_x, 0].max
      elsif @skew_x<0 then
        @skew_x=[@skew_x+@stabilize_per_update_x, 0].min
      end
      if @skew_y>0 then
        @skew_y=[@skew_y-@stabilize_per_update_y, 0].max
      elsif @skew_y<0 then
        @skew_y=[@skew_y+@stabilize_per_update_y, 0].min
      end
    end
  end
  
  def skew_x_by_slot(slot)
    return 0 if slot==0
    return skew_x_by_slot(slot-1) if @stack[slot].get_param_bool('stable')
    return skew_x_by_slot(slot-1)+height_of_slot(slot-1)*@skew_x
  end
  
  def skew_y_by_slot(slot)
    return 0 if slot==0
    return skew_y_by_slot(slot-1) if @stack[slot].get_param_bool('stable')
    return skew_y_by_slot(slot-1)+height_of_slot(slot-1)*@skew_y
  end
  
  def distance_to_pivot
    return Math.sqrt( (@carrier.real_x-@pivot_x)**2 + (@carrier.real_y-@pivot_y)**2 )
  end
  
  def update_slots
    @stack.each_index do |slot|
      update_slot(slot)
    end
  end
  
  def update_slot(slot)
    return if @stack[slot].charpath?
    @stack[slot].draw_at( \
     @pivot_x, @pivot_y, \
     PixelShift.to_pixels(skew_x_by_slot(slot)), \
     PixelShift.to_pixels(-height_by_slot(slot)*@compress+skew_y_by_slot(slot)) \
    )
  end
  
end