# Eng/Англ: Adds the sign method to Numeric types.
# Rus/Рус: Добавляет метод sign в объекты чисел, возвращающий знак числа.
class Numeric
  def sign
    self<=>0
  end
end
