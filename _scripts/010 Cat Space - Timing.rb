# Eng/Англ: This 'abstract' class controls process timing.
# Rus/Рус: Этот "абстрактный" класс управляет динамикой прогресса.
class Timing
  include Setupable
  include Startable
  include Disposable
  
  DIR_FORWARD =0 # константа - движение вперёд
  DIR_BACKWARD=1 # константа - движение назад
  
  attr_accessor :repeat # количество повторений
  attr_reader   :delay  # задержка (кадры)
  attr_reader   :dir    # направление
  attr_reader   :alternate  # чередовать направление
  attr_reader   :counter    # счётчик
  attr_reader   :delay_counter  # счётчик задержки
  
  def initialize(duration, delay=0)
    self.counter=0
    @duration=duration
    @delay=0
    @paused=false
    @delay_counter=0
    @repeat=false
    @dir=DIR_FORWARD
    @alternate=false
    @bound={}
  end
  
  def setup_args  # аргументы установщика
    [:repeat, :delay, :paused, :alternate, :dir, :duration]
  end
  
  def bind(event) # подписка
    @bound[event]=[] unless @bound.has_key?(event)
    @bound[event].push(Proc.new)
  end
  
  def update    # обновить
    return if finished? or paused? or disposed?
    if delayed?
      tick_delay
      return
    end
    start unless started?
    tick_counter
    finish if finished?
  end
  
  def finished? # завершено?
    counter>=@duration
  end
  
  def pause     # на паузу
    @paused=true
  end
  
  def unpause   # с паузы
    @paused=false
  end
  
  def toggle_pause  # переключить паузу
    @paused=!@paused
  end
  
  def paused?   # на паузе?
    @paused
  end
  
  def delayed?  # задержка?
    @delay>0 and @delay_counter<@delay
  end
  
  def repeat?   # повторить?
    return @repeat if @repeat.is_a?(Boolean)
    return @repeat>1
  end
  
  def alternate? # чередовать?
    @alternate
  end
  
  def forward?  # вперёд?
    @dir==DIR_FORWARD
  end
  
    def percent # ползунок
    @percent=calc_percent if @percent.nil?
    return @percent
  end
  
  def execute_dispose   # убрать
    @bound=nil
  end
  
  protected
  
  def tick_delay    # отсчёт задержки
    @delay_counter+=1
  end
  
  def tick_counter  # отсчёт ползунка
    self.counter+=1
    unless finished?
      inform(:tick)
    end
  end
  
  def start         # начать
    @started=true
    inform(:start)
  end
  
  def finish        # завершить
    if repeat?
      rewind
      inform(:rewind)
      inform(:tick)
    else
      inform(:finish)
    end
  end
  
  def rewind        # перемотать
    self.counter=0
    @repeat-=1 if @repeat.is_a?(Numeric)
    if alternate?
      if forward?
        @dir=DIR_BACKWARD
      else
        @dir=DIR_FORWARD
      end
    end
  end
  
  def calc_percent  # подсчитать ползунок
    base_percent
  end
  
  def base_percent  # базовый ползунок
    bp=counter.to_f/@duration
    bp=1-bp unless forward?
    return bp
  end
  
  def inform(event) # проинформировать
    return unless @bound.has_key?(event)
    @bound[event].each { |proc| proc.call }
  end
  
  def counter=(val) # счётчик
    @counter=val
    @percent=nil
  end
end

# Eng/Англ: The 'abstract' class for Timing with strength parameter.
# Rus/Рус: Абстрактный класс для прогресса, имеющего выраженность.
class Timing_with_strength < Timing
  DEFAULT_STRENGTH=2    # выраженность по умолчанию.
  
  attr_reader :strength # выраженность
  
  def setup_args
    super << :strength
  end
  
  def initialize(*args)
    super
    @strength=DEFAULT_STRENGTH
  end
  
  def calc_percent
    apply_strength(base_percent, @strength)
  end
  
  def apply_strength(percent, strength)
    percent
  end
end
  
# Eng/Англ: The class for ease-in process.
# Rus/Рус: Класс для нарастающего процесса
class Timing_Ease_In < Timing_with_strength
  def apply_strength(percent, strength)
    percent**strength
  end
end

# Eng/Англ: The class for ease-out process.
# Rus/Рус: Класс для замедляющегося процесса
class Timing_Ease_Out < Timing_with_strength
  def apply_strength(percent, strength)
    percent**(1.0/strength)
  end
end