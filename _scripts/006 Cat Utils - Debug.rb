# Eng/Англ: A basic utility script for debug purposes.
# Rus/Рус: Простой скрипт с функциями, полезными для отладки.

# привычное по php и некоторым другим языкам название функции.
alias :var_dump :msgbox_p

# возможность из любого места вызвать halt, чтобы на 10 секунд остановить
# всякое выполнение (например, чтобы посмотреть изменения во фрейме).
module Kernel
  def halt(secs=10)
    p 'Halting!'
    sleep secs
    p 'Resuming!'
  end
end

# учит объекты представляться комбинацией класса и уникального айди.
class Object
  def introduce
    self.class.name+self.object_id
  end
end

# приказать представиться всем объектам в массиве или хэше.
module Enumerable
  def introduce_elements
    self.each { |value| p value.introduce }
  end
end

# приспособление на случай, если нужно вывести содержание obj.inspect
# во внутриигровом сообщении, поскольку после \ иногда может идти нуль.
class Window_Message
  alias :varDump_process_escape_character :process_escape_character
  
  def process_escape_character(code, text, pos)
    return if code==nil
    varDump_process_escape_character(code, text, pos)
  end
end


# добавляет точку для создания тестового действия по F5.
# dependancy/зависимость: Cat Controls - Custom
class Game_Player
  alias :varDump_check_custom_input :check_custom_input
  
  def check_custom_input
    return true if varDump_check_custom_input
    return do_test_action if Input.press?(:F5)
    return false
  end
  
  # точка обработки тестового действия. возвращает либо ложь, если обработки
  # не было, либо истину, если была.
  def do_test_action
    return false
  end
end