# Eng/Англ: A utility script for better access to self switches.
# Rus/Рус: Скрипт для удобства доступа к переключателям событий.

class Game_Event
  
  def self_switch_key(switch)
    [@map_id, @id, switch]
  end
    
  def self_switch(switch)
    $game_self_switches[self_switch_key(switch)]
  end
  
  def set_self_switch(switch, value)
    return if self_switch(switch)==value
    $game_self_switches[self_switch_key(switch)]=value
    $game_map.need_refresh=true
  end
  
  def self_switch_on(switch)
    set_self_switch(switch, true)
  end
  
  def self_switch_off(switch)
    set_self_switch(switch, false)
  end
end