# Eng/Англ: Additional conditions for selecting event's page by specially
# formed comments at the start of a page.

# Rus/Рус: Дополнительные условия для выбора страницы события, задающиеся
# особыми комментариями внизу страницы.

class Game_Event

  SPC_EX=/^\#condition (self_switch)(\:(.+))?\s*$/
  
  alias :specialPageConditions_conditions_met? :conditions_met?
  
  def conditions_met?(page)
    pre=specialPageConditions_conditions_met?(page)
    return false unless pre
    
    commands=page.list
    index=0
    while command=commands[index] do
      break unless command.code==108 \
        or command.code==408 # comment or comment continuation
      break unless SPC_EX=~command.parameters[0]
      return false unless parse_special_condition($~[1], $~[3])
      index+=1
    end
    return true
  end
  
  def parse_special_condition(condition_type, condition_text)
    func=method('parse_special_condition_'+condition_type)
    func.call(condition_text)
  end
  
  def parse_special_condition_self_switch(switch_text)
    return self_switch(switch_text.to_i) if switch_text.match(/^\d+$/)
    return self_switch(switch_text.to_sym)
  end
end

