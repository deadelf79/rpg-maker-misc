#| <template>
#| 
#| 
#| 
#| 
#| 

class EventTemplate
	include Setupable
	
	DEFAULT_DATA={}
	
	attr_reader :event
	
	def initialize(event, data)
		@event=event
		setup(default_data+data)
	end
	
	def default_data
		DEFAULT_DATA
	end
	
	def setup_arg?(sym)
		default_data.has_key?(sym)
	end

end

class EventTemplate_Ingredient < EventTemplate

	DEFAULT_DATA=
	{
		category: :mystery
	}
	
end

class Game_Event

	alias :eventTemplate_init_private_members :init_private_members
	def init_private_members
		eventTemplate_init_private_members
		@templates=[]
	end

	def apply_template(template_code, template_data)
		template=Object.const_get('EventTemplate_'+template_code).new(self, template_data)
		@templates.push(template)
	end
	
	def get_param(code)
		@templates.each { |template| return template.get_param(code) if template.has_param?(code) }
	end
	
	def respond_to?(method_name)
		return true if super
		@templates.each { |template| return true if template.respond_to?(method_name) }
		return false
	end
	
	def method_missing(method_name, *args)
    if @templates
      @templates.each { |template| return template.send(method_name, *args) if template.respond_to?(method_name) }
    end
		super
	end
end