# WIP
# Этот скрипт представляет персонажа на экране как цилиндр определённой высоты
# с определённым основанием. Необходимо для того, чтобы правильно класть
# одни предметы на другие, где бы они ни находились в своей картинке тайла.

class Game_CharacterBase
  
  CELL_WIDTH=32
  
  def pivot_relative_x
    return px-CELL_WIDTH/2 if px=get_param_pixels('pivot_x', nil)
    return 0
  end
  
  def pivot_relative_y
    return py-0.5 if py=get_param_length('pivot_y', nil)
    return -0.5 unless object_character?
    return 0
  end
  
  def pivot_x
    @real_x+pivot_relative_x
  end
  
  def pivot_y
    @real_y+pivot_relative_y
  end
  
  def height
    get_param_length('height', 1)
  end
  
  def top_x
    pivot_x
  end
  
  def top_y
    top_y-height
  end
  
  def draw_pivot_at(x, y)
    
  end
end