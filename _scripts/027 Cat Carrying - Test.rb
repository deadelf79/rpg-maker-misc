# Тестирование стопки-башни.

class Game_Player
  
  def init_carrier
    @stack=CarryStack_Tower.new(self)
  end
  
  def carrier?
    @stack.to_bool
  end
  
  def push_stacks(stacks)
    stacks.push(@stack)
  end
  
  def carry(thing)
    unless @stack.add(thing)
      Sound.play_buzzer
      var_dump ':('
    end
  end
  
  def expel(thing)
    @stack.expel(thing)
  end
  
end