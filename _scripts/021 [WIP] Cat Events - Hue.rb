# WIP
# Скрипт для перекрашивания персонажей, находящихся на экране.

class Game_CharacterBase
  attr_accessor :hue_shift
  
  alias :eventHue_initialize :initialize
  def initialize(*args)
    @hue_shift=0
    eventHue_initialize(*args)
  end
end

class Sprite_Character
  attr_reader :hue_shift
  
  alias :eventHue_initialize :initialize
  def initialize(*args)
    @hue_shift=0
    eventHue_initialize(*args)
  end
  
  alias :eventHue_update :update
  def update
    eventHue_update
    update_hue if @character.hue_shift!=@hue_shift
  end
  
  def update_hue
    self.bitmap.hue_change(@character.hue_shift-@hue_shift)
    @hue_shift=@character.hue_shift
  end
end