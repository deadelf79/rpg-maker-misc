# Eng/Англ: Common mixins.
# Rus/Рус: Модули для включения в классы.

# Методы для классов, которые могут получить установки, в особенности если
# они могут получить эти установки только до начала связанного процесса.
module Setupable
  
  # задать установки
  # можно передавать готовый хэш, а можно вызывать таким образом:
  #   obj.setup(animal: 'cat', cry: 'meow', claws: 4)
  # причём эти именованные параметры можно перечислять в любом порядке.
  def setup(opts={}) 
    raise 'Setup after start!' if respond_to?(:started?) and started?
    opts.each { |name|
      next unless setup_arg?(name)
      
      # старается задать параметры процедурно и только затем - локально.
      # на случай, если изменение параметров должно влечь за собой
      # дополнительные последствия.
      if self.respond_to?(name.id2name+'=')
        send(name.id2name+'=', opts[name])
      else
        self.instance_variable_set('@'+name.id2name, opts[name])
      end
    }
  end
  
  # этот метод должен быть возвращать массив с аргументами, возможными
  # для установки через setup.
  def setup_args
    []
  end
  
  def setup_arg?(sym)
    setup_args.include?(sym)
  end
  
end

# Методы для классов, которые могут быть "начаты" и "не начаты", то есть
# описывающих процессы.
module Startable
  def started?
    @started.to_b
  end
  
  def start
    @started=true
  end
end

# Методы для классов, которые должны  быть убраны после завершения работы
# с ними.
module Disposable
  def disposed?
    @disposed.to_b
  end
  
  def dispose
    return if disposed?
    @disposed=true
    execute_dispose
  end
  
  def execute_dispose
  end
end

# Методы для классов, объекты которых должны обновляться пофреймово.
# WIP
module Updatable
  
  def subupdates
    @subupdates=[] if @subupdates.nil?
    return @subupdates
  end
  
  def update?
    @to_update.to_b
  end
  
  def update
  end
  
  def updates_on
    @to_update=true
  end
  
  def updates_off
    @to_update=false
  end
  
end