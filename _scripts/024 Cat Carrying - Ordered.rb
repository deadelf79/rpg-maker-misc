# Это "абстрактный" класс для стопок, содержимое которых однородно за исключением
# порядка следования. Например, башня на голове у персонажа.

class CarryStack_Ordered < CarryStack
  MAX_STACK_LIMIT   =10
  DEFAULT_MAX_STACK =3
  
  def initialize(max_stack=DEFAULT_MAX_STACK)
    super()
    @max_stack=max_stack
  end
  
  def create_stack
    Array.new
  end
  
  def max_stack # максимальный_размер
    return @max_stack
  end
 
  def max_stack=(val)
    raise 'too large stack size suggested' if val>MAX_STACK_LIMIT
    raise 'stack size smaller than current suggested' if val<@stack.size
    raise 'stack size 0 suggested' if val<1
    @max_stack=val
  end
 
  def full?
    @stack.size>=@max_stack
  end

  def find_slot_for(thing)
    return if full?
    size
  end
  
  def valid_slot?(slot)
    slot >= 0 \
     and slot <= size \
     and slot < max_stack
  end
  
  def free_slot?(slot)
    slot==size
  end

  def insert(thing, slot)
    @stack[slot]=thing
  end
  
  def remove(thing)
    @stack.delete(thing)
  end
  
  def pad(thing) # надставить
    add(thing, 0)
  end
  
  def pop # снять
    expel top
  end
  
  def shift # вытащить
    expel bottom
  end
  
  def top(count=1) # сверху
    return false if count>size
    return @stack[size-1] if count==1
    @stack[-1, count]
  end
  
  def bottom(count=1) # снизу
    return false if count>size
    return @stack[0] if count==1
    @stack[0, count]
  end
  
  def top?(num=1, &block) #верхние? (проверка блоком)
    return size>=num unless block_given?
    @stack[-1, num].all?(&block)
  end
  
  def bottom?(num=1, &block) #нижние? (проверка блоком)
    return size>=num unless block_given?
    @stack[0, num].all?(&block)
  end
  
end