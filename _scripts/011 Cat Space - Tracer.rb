# Eng/Англ: The 'abstract' class representing trajectory.
# Rus/Рус: "Абстрактный" класс, отвечающий за траекторию.
class Tracer  
  include Setupable
  include Startable
  include Disposable
  
  def initialize
    @cache={}
    self.percent=0.0
    bind_calculators
  end
  
  attr_reader :percent # ползунок
  def percent=(val)
    @percent=val
    @cached=false
  end
  
  def bind_calculators # привязка вычислителей
    @calc={}
    axes.each { |axis|
      method_name='calc_'+axis.id2name
      @calc[axis]=method(method_name) if respond_to?(method_name)
    }
  end
  
  def cached?   # вычислено?
    @cached
  end
  
  def axes      # оси
    [:x, :y]
  end
  
  def valid_axis?(axis) # настоящая ось?
    axes.include?(axis)
  end
  
  def method_missing(sym, *args)
    return get_coord(sym, *args) if valid_axis?(sym)
    super
  end
  
  def respond_to?(sym, include_private = false)
    super or valid_axis?(sym)
  end
  
  def get_coord(axis, for_percent=nil) # получить координату
    # raise 'Invalid axis' unless valid_axis?(axis) # excessive ops / лишняя операция
    cacheable=for_percent.nil? or for_percent==@percent
    for_percent=@percent if for_percent.nil?
    return @cache[axis] if cacheable and cached?
    
    coords=calc_coords(for_percent)
    @cache, @cached = coords, true if cacheable
    return coords[axis]
  end
  
  def calc_coords(percent) # вычислить координаты
    result={}
    axes.each { |axis| result[axis]=@calc[axis].call(percent) }
    return result
  end
  
end

# Eng/Англ: The circle Trajectory.
# Rus/Рус: Класс круговой траектории.
class Trace_Circle < Tracer
  attr_reader   :start_point  # начальная точка
  attr_reader   :center_point # центральная точка
  attr_reader   :cw           # направление вращения
  attr_reader   :radius       # радиус
  attr_reader   :start_angle  # начальный угол
  
  def setup_args
    [:cw]
  end
  
  def initialize(opts={})
    super()
    
    if opts.has_key?(:start_point) 
      @start_point=opts[:start_point]
    elsif opts.has_key?(:start_x) and opts.has_key?(:start_y)
      @start_point=Point_Fixed.new(opts[:start_x], opts[:start_y])
    end
    
    if opts.has_key?(:radius) and opts.has_key?(:start_angle)
      @radius=opts[:radius]
      @start_angle=opts[:start_angle]
    end
    
    if opts.has_key?(:center_point) 
      @center_point=opts[:center_point]
    elsif opts.has_key?(:center_x) and opts.has_key?(:center_y)
      @center_point=Point_Fixed.new(opts[:center_x], opts[:center_y])
    end
    
    if @start_point and @radius and @start_angle
      calc_center
    elsif @start_point and @center_point and not @radius and not @start_angle
      calc_polar
    elsif @center_point and @radius and @start_angle
      # nop
    else
      raise 'Insufficient circle data'
    end
    
    @cw=opts.has_key?(:cw)? opts[:cw] : true
    
  end
  
  def axes
    super << :angle
    # angle should be calculated first
    # угол должен вычисляться первым.
  end
  
  def calc_polar  # вычислить полярные параметры
    @radius=Math.sqrt( \
      (@center_point.x-@start_point.x)**2 + \
      (@center_point.y-@start_point.y)**2 )
    @start_angle=Math.asin( (@start_point.y-@center_point.y)/@radius)
    @start_angle=Math::PI-@start_angle if @start_point.x<@center_point.x
  end
  
  def calc_center # вычислить центр
    @center_point=Point_Fixed.new( \
      @start_point.x-Math.cos(@start_angle)*@radius, \
      @start_point.y+Math.sin(@start_angle)*@radius)
  end
  
  def calc_coords(percent)
    angle=@start_angle+Math::PI*2*percent*( @cw ? 1 : -1 )
    x=@center_point.x+Math.cos(angle)*@radius
    y=@center_point.y-Math.sin(angle)*@radius
    return {angle: angle, x: x, y: y}
  end
end

class Trace_Arc < Tracer
  attr_reader   :start_point
  attr_reader   :end_point
  attr_reader   :height
  
  DEFAULT_HEIGHT=1
  
  def setup_args
    [:height]
  end
  
  def initialize(opts={})
    super()
    
    if opts.has_key?(:start_point) 
      @start_point=opts[:start_point]
    elsif opts.has_key?(:start_x) and opts.has_key?(:start_y)
      @start_point=Point_Fixed.new(opts[:start_x], opts[:start_y])
    else
      raise('No start point')
    end
    
    if opts.has_key?(:end_point) 
      @end_point=opts[:end_point]
    elsif opts.has_key?(:end_x) and opts.has_key?(:end_y)
      @end_point=Point_Fixed.new(opts[:end_x], opts[:end_y])
    else
      raise('No end point')
    end
    
    @height=opts[:height] || DEFAULT_HEIGHT

  end
  
  def calc_x(percent)
    @start_point.x + (@end_point.x-@start_point.x)*percent
  end
  
  def calc_y(percent)
    @start_point.y + \
    (@end_point.y-@start_point.y)*percent - \
    @height*Math.sin(percent*Math::PI)
  end
      
end