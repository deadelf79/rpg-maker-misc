# Eng/Англ: Script Command now has 'context' method call
# which returns the Event owning the Command.
# It works even if the map has changed (as opposed to standard behavior)
# IMPORTANT! If the player exits the map and thens returns during
# the course of the Interpreter, the map will have a different copy
# of the same Event.

# Rus/Рус: Теперь в коде команды "Script" у событий можно получить
# ссылку на событие, вызвавшее выполнение скрипта.
# Работает даже если игрока переместили на другую карту.
# ВНИМАНИЕ! Если интерпретатор переместит игрока на другую карту,
# а потом вернёт на ту же, то ею будет использоваться другая копия
# того же события.

class Game_Interpreter
 
  alias :contextEvent_setup :setup
  attr_reader :context
  
  def setup(list, event_id = 0)
    preserve_context(event_id)
    contextEvent_setup(list, event_id)
  end
 
  def preserve_context(event_id)
    # Eng/Англ: Interperter is always called by an event from current map
    # Rus/Рус: интерпретатор всегда вызывается событием текущей карты.
    @context = event_id>0 ? $game_map.events[event_id] : nil
  end
 
end